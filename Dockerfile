FROM alpine:edge

RUN apk add --no-cache \
    --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
    bash curl jq jo

ADD _gitlab/release.sh release.sh

ENV DEBUG=1 DRY=1 GLR_TOKEN=a CI_PROJECT_ID=1 CI_PROJECT_PATH=nobbz_dev/home CI_COMMIT_TAG=v0.0.0

RUN ./release.sh