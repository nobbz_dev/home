#!/usr/bin/env bash

set -e

# enable debug if DEBUG is non-empty
[ -z "${DEBUG}" ] || set -x

GLR_BASE=${GLR_BASE:="https://gitlab.com"}
GLR_API=${GLR_APR:="${GLR_BASE}/api/v4"}
GLR_REL_PATH=${GLR_REL_PATH:="release"}

CURL_OPTS=()

[ ! -z "${DEBUG}" ] || CURL_OPTS+=(-s)

function curl_stub () {
    if [ -z "${DEBUG}" ]; then
        echo curl "${CURL_OPTS}" "${@}"
    else
        curl ${CURL_OPTS} "${@}"
    fi
}

function missing_tool () {
    printf "[ERROR] '%s' needs to be installed\n" "${1}"
    exit 1
}

function missing_var () {
    printf "[ERROR] %s needs to be set in the environment\n" "${1}"
    exit 2
}

function check_release () {
    printf "[INFO ] Checking for existing release on tag %s\n" "${CI_COMMIT_TAG}"
    curl_stub --fail --header "PRIVATE-TOKEN: ${GLR_TOKEN}" "${GLR_API}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}" || return 1
}

function create_release () {
    printf "[INFO ] Creating release for tag %s" "${CI_COMMIT_TAG}"
    PAYLOAD=$(jo id=${CI_PROJECT_ID} name=${CI_COMMIT_TAG} tag_name=${CI_COMMIT_TAG} description="Autorelease for ${CI_COMMIT_TAG}")
    curl_stub --fail --header "PRIVATE-TOKEN: ${GLR_TOKEN}" --header 'Content-Type: application/json' -XPOST "${GLR_API}/projects/${CI_PROJECT_ID}/releases" --data "${PAYLOAD}" || return 1
}

for p in curl jq jo; do
    if ! which ${p} 2>&1 >/dev/null; then missing_tool "${p}"; fi
done

for v in GLR_TOKEN CI_PROJECT_ID CI_PROJECT_PATH CI_COMMIT_TAG; do
    if [ -z "${!v}" ]; then missing_var "${v}"; fi
done


check_release || create_release

for f in $(ls ${GLR_REL_PATH}); do
    LINK_ID=$(curl ${CURL_OPTS} --header "PRIVATE-TOKEN: ${GLR_TOKEN}" "${GLR_API}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links" | jq -r --arg FILE "${f}" '.[] | select(.name == $FILE) | .id')
    [ ! -z "${LINK_ID}" ] || printf "[WARN ] Asset '%s' already exists, overwriting/updating with current" "${f}"
    printf "[INFO ] Uploading %s/%s to GitLab." "${GLR_REL_PATH}" "${f}"
    $(curl $CURL_OPTS --fail --header "PRIVATE-TOKEN: ${GLR_TOKEN}" -XPOST --form "file=@${GLR_REL_PATH}/${f}" "${GLR_API}/projects/${CI_PROJECT_ID}/uploads" | jq -r .url)
    if [ ! -z "${LINK_ID}" ]; then
        printf "[INFO ] Updating asset link on release"
        curl $CURL_OPTS --fail --header "PRIVATE-TOKEN: ${GLR_TOKEN}" -XPUT --data url="${GLR_BASE}/${CI_PROJECT_PATH}/${ASSET_URL}" "${GLR_API}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links/${LINK_ID}"
    else
        printf "[INFO ] Attaching asset to release"
        curl $CURL_OPTS --fail --header "PRIVATE-TOKEN: ${GLR_TOKEN}" -XPOST --data name="${f}" --data url="${GLR_BASE}/${CI_PROJECT_PATH}/${ASSET_URL}" "${GLR_API}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links" | jq .
    fi
done