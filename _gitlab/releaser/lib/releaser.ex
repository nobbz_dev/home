defmodule Releaser do
  require Logger

  alias Releaser.Config
  alias Releaser.GitLab

  @apps [:logger]

  def main([]), do: main(["release"])

  def main([release_folder]) when is_binary(release_folder) do
    Enum.each(@apps, &Application.ensure_all_started/1)

    Logger.info("Reading configuration from environment")

    config =
      case Config.read(release_folder) do
        {:ok, config} ->
          config

        {:error, missing} ->
          missing
          |> Enum.each(fn var ->
            Logger.error("Required environment variable '#{var}' is missing")
          end)

          halt(1)
      end

    Logger.info("Read config data")

    Logger.info("Checking if release '#{config.commit_tag}' exists")

    config
    |> GitLab.release_exists?()
    |> (&(not &1)).()
    |> if do
      Logger.info("Create release '#{config.commit_tag}'")

      case GitLab.create_release(config) do
        :ok ->
          :ok

        other ->
          Logger.error("Unknown response when creating the release", response: inspect(other))
          halt(2)
      end
    end

    Logger.info("Prepared release")

    Logger.info("Uploading files")

    {:ok, files} =
      config.release_path
      |> Path.expand()
      |> File.ls()

    links =
      case GitLab.get_links(config) do
        {:ok, links} -> links
      end

    files
    |> Stream.map(fn file ->
      links
      |> Enum.find(fn
        %{"name" => ^file} -> true
        _ -> false
      end)
      |> case do
        nil -> {:create, file, %{}}
        map when is_map(map) -> {:update, file, map}
      end
    end)
    |> Stream.map(fn {cmd, file, map} ->
      Logger.info("Uploading file #{file}")
      {:ok, url} = GitLab.upload_file(config, file)
      {cmd, file, map, url}
    end)
    |> Stream.map(fn
      {:create, file, map, url} when is_binary(url) ->
        Logger.info("Create new asset link for #{file} (#{url})")
        :ok = GitLab.create_link(config, file, url)
        Logger.info("Successfully created link for #{file}")

      {:update, file, map, url} when is_binary(url) ->
        Logger.warn("Overwrite existing asset link for #{file} (#{url})")
        :ok = GitLab.update_link(config, map["id"], file, url)
        Logger.info("Successfully updated link for #{file}")
    end)
    |> Stream.run()
  end

  def halt(status \\ 0) do
    # Give other processes the chance to process their last messages
    Process.sleep(100)
    System.halt(status)
  end
end
