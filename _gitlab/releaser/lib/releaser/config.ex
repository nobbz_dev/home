defmodule Releaser.Config do
  require Logger

  defstruct release_path: "release",
            token: nil,
            project_id: nil,
            project_path: nil,
            commit_tag: nil

  @var_map %{
    "GLR_TOKEN" => :token,
    "CI_PROJECT_ID" => :project_id,
    "CI_PROJECT_PATH" => :project_path,
    "CI_COMMIT_TAG" => :commit_tag
  }

  def read(release_folder) do
    @var_map
    |> Enum.map(fn {var, _} -> var end)
    |> Enum.reduce({%__MODULE__{}, []}, fn var, {conf, failures} ->
      case System.get_env(var) do
        nil ->
          {conf, [var | failures]}

        value when is_binary(value) ->
          {%{conf | @var_map[var] => value}, failures}
      end
    end)
    |> case do
      {conf, []} -> {:ok, %{conf | release_path: release_folder}}
      {_, failures} -> {:error, failures}
    end
  end
end
