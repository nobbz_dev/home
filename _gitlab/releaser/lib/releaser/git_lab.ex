defmodule Releaser.GitLab do
  use Tesla, only: [:get, :post], docs: false

  alias Tesla.Middleware, as: MW
  alias Tesla.Multipart

  @base_url Application.get_env(:releaser, :base_url, "https://gitlab.com/api/v4")
  @token_field "private-token"

  plug MW.BaseUrl, @base_url
  plug MW.JSON

  def release_exists?(config) do
    "/projects/#{config.project_id}/releases/#{config.commit_tag}"
    |> get(headers: [{@token_field, config.token}])
    |> case do
      {:ok, %Tesla.Env{status: 403}} -> false
      {:ok, %Tesla.Env{status: 200}} -> true
    end
  end

  def create_release(config) do
    data = %{
      tag_name: config.commit_tag,
      name: config.commit_tag,
      description: "Autorelease for #{config.commit_tag}"
    }

    "/projects/#{config.project_id}/releases"
    |> post(data, headers: [{@token_field, config.token}])
    |> case do
      {:ok, %Tesla.Env{status: status}} when status in 200..299 -> :ok
      other -> other
    end
  end

  def get_links(config) do
    "/projects/#{config.project_id}/releases/#{config.commit_tag}/assets/links"
    |> get(headers: [{@token_field, config.token}])
    |> case do
      {:ok, %Tesla.Env{body: body}} when is_list(body) -> {:ok, body}
    end
  end

  def upload_file(config, file) do
    body =
      Multipart.new()
      |> Multipart.add_file(config.release_path |> Path.expand() |> Path.join(file), name: :file)

    "/projects/#{config.project_id}/uploads"
    |> post(body, headers: [{@token_field, config.token}])
    |> case do
      {:ok, %Tesla.Env{body: %{"url" => url}}} -> {:ok, url}
    end
  end

  def create_link(config, name, "/" <> url), do: create_link(config, name, url)

  def create_link(config, name, url) do
    file_url = "https://gitlab.com/#{config.project_path}/#{url}"
    path = "/projects/#{config.project_id}/releases/#{config.commit_tag}/assets/links"

    Tesla.client([{MW.BaseUrl, @base_url}, MW.FormUrlencoded])
    |> Tesla.post(path, %{name: name, url: file_url}, headers: [{@token_field, config.token}])
    |> case do
      {:ok, %Tesla.Env{status: status}} when status in 200..299 -> :ok
    end
  end

  def update_link(config, id, name, "/" <> url), do: update_link(config, id, name, url)

  def update_link(config, id, name, url) do
    file_url = "https://gitlab.com/#{config.project_path}/#{url}"
    path = "/projects/#{config.project_id}/releases/#{config.commit_tag}/assets/links/#{id}"

    Tesla.client([{MW.BaseUrl, @base_url}, MW.FormUrlencoded])
    |> Tesla.put(path, %{url: file_url}, headers: [{@token_field, config.token}])
    |> case do
      {:ok, %Tesla.Env{status: status}} when status in 200..299 -> :ok
    end
  end
end
