# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :nobbz_dev_home,
  ecto_repos: [NobbzDevHome.Repo],
  source_base: "https://gitlab.com/nobbz_dev/home/tree",
  commit_sha: System.get_env("CI_COMMIT_SHA") || nil

# Configures the endpoint
config :nobbz_dev_home, NobbzDevHomeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "3eH3Jp1gLMGnka5pa+2mDO3vdm4CQ10SGIc+Zfeb9EQiPnDyXrBBefwcLDaZt19b",
  render_errors: [view: NobbzDevHomeWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: NobbzDevHome.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
