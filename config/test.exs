use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :nobbz_dev_home, NobbzDevHomeWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :nobbz_dev_home, NobbzDevHome.Repo,
  username: "postgres",
  password: "postgres",
  database: "nobbz_dev_home_test",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox
