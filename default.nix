{ pkgs ? import <nixpkgs> {}}:

with pkgs;

let elixir = beam.packages.erlangR22.elixir_1_8;
    # elixir = (beam.packagesWith erlang).elixir_1_8;
in mkShell {
  buildInputs = [ elixir git ];
}