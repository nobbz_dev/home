defmodule NobbzDevHome.ReleaseTasks do
  @start_apps [:crypto, :ssl, :postgrex, :ecto, :ecto_sql]

  @repos Application.get_env(:nobbz_dev_home, :ecto_repos, [])

  alias Ecto.Migrator

  require Logger

  def migrate(_args) do
    start_logger()
    start_services()
    run_migrations()
    stop_services()
  end

  defp start_logger(), do: Application.ensure_all_started(:logger)

  defp start_services() do
    Logger.info("Starting dependencies")

    Enum.each(@start_apps, fn app ->
      Logger.info("Starting #{app}")
      Application.ensure_all_started(app)
    end)

    Logger.info("Starting repositories")

    Enum.each(@repos, fn repo ->
      Logger.info("Starting #{repo}")
      repo.start_link(pool_size: 2)
    end)
  end

  defp stop_services() do
    Logger.info("Success")
    :init.stop()
  end

  defp run_migrations() do
    Logger.info("Running migrations")
    Enum.each(@repos, &run_migrations_for/1)
  end

  defp run_migrations_for(repo) do
    app = Keyword.get(repo.config(), :otp_app)
    Logger.info("Running migrations for #{app}")
    migrations_path = priv_path_for(repo, "migrations")
    Migrator.run(repo, migrations_path, :up, all: true)
  end

  defp priv_path_for(repo, filename) do
    app = Keyword.get(repo.config(), :otp_app)

    priv_dir = app |> :code.priv_dir() |> to_string()

    repo_underscore = repo |> Module.split() |> List.last() |> Macro.underscore()

    Path.join([priv_dir, repo_underscore, filename])
  end
end
