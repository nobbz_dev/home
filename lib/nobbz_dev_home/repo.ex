defmodule NobbzDevHome.Repo do
  use Ecto.Repo,
    otp_app: :nobbz_dev_home,
    adapter: Ecto.Adapters.Postgres
end
