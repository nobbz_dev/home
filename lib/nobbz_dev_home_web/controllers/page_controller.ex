defmodule NobbzDevHomeWeb.PageController do
  use NobbzDevHomeWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
