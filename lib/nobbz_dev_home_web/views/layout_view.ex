defmodule NobbzDevHomeWeb.LayoutView do
  use NobbzDevHomeWeb, :view

  @sha Application.get_env(:nobbz_dev_home, :commit_sha)
  @version_str NobbzDevHome.MixProject.project()[:version]
  @version @version_str |> Version.parse!()

  @major @version.major
  @minor @version.minor
  @patch @version.patch

  @vsn "#{@major}.#{@minor}.#{@patch}"

  if @sha do
    @sourcebase Application.get_env(:nobbz_dev_home, :source_base)
    def version_info do
      link(
        @vsn,
        to: "#{@sourcebase}/#{@sha}",
        data: [{:tooltip, nil}],
        title: to_string(@version_str)
      )
    end
  else
    def version_info, do: @version_str
  end
end
