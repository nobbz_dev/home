defmodule NobbzDevHomeWeb.PageControllerTest do
  use NobbzDevHomeWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to NobbZ"
  end
end
